defmodule Board do
  @empty_cell ' '

  def new do
    [
      [@empty_cell, @empty_cell, @empty_cell],
      [@empty_cell, @empty_cell, @empty_cell],
      [@empty_cell, @empty_cell, @empty_cell]
    ]
  end

  def register_move(board, _sign, {x, y}) when x < 0 or x >= 3 or y < 0 or y >= 3 do
    {:invalid_play, board}
  end

  def register_move(board, sign, position) do
    if empty_cell?(board, position) do
      new_board = update(board, sign, position)
      {check_status(new_board, sign), new_board}
    else
      {:cell_taken, board}
    end
  end

  def print(board) do
    board
    |> Enum.map(&Enum.join(&1, " | "))
    |> Enum.join("\n----------\n")
  end

  defp update(board, sign, {x, y}) do
    board
    |> List.update_at(x, &List.update_at(&1, y, fn(_) -> sign end))
  end

  defp empty_cell?(board, {x, y}) do
    current_value =
      board
      |> Enum.at(x)
      |> Enum.at(y)

    current_value == @empty_cell
  end

  defp check_status(board, sign) do
    case board do
      [[^sign, ^sign, ^sign], _, _] ->
        :game_over

      [_, [^sign, ^sign, ^sign], _] ->
        :game_over

      [_, _, [^sign, ^sign, ^sign]] ->
        :game_over

      [[^sign, _, _], [^sign, _, _], [^sign, _, _]] ->
        :game_over

      [[_, ^sign, _], [_, ^sign, _], [_, ^sign, _]] ->
        :game_over

      [[_, _, ^sign], [_, _, ^sign], [_, _, ^sign]] ->
        :game_over

      [[^sign, _, _], [_, ^sign, _], [_, _, ^sign]] ->
        :game_over

      [[_, _, ^sign], [_, ^sign, _], [^sign, _, _]] ->
        :game_over

      _ ->
        case board |> List.flatten() |> Enum.any?(&(&1 == ?\s)) do
          true -> :continue
          _ -> :draw
        end
    end
  end
end
