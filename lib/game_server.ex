defmodule GameServer do
  use GenServer

  # Notifica jogares de que o jogo acabou.
  # Quando acabar, se mata!

  @player_one_sign 'X'
  @player_two_sign 'O'

  def start_link(_) do
    GenServer.start_link(__MODULE__, [])
  end

  def join(pid, player_pid) do
    GenServer.call(pid, {:join, player_pid})
  end

  def player_move(pid, position) do
    GenServer.call(pid, {:player_move,  position})
  end

  def show_board(pid) do
    GenServer.call(pid, :show_board)
  end

  # Callbacks

  def init(_) do
    {:ok, %{board: Board.new(), players: [], current_player: nil}}
  end

  def handle_call({:player_move, _}, {pid1, _ref}, state = %{current_player: pid2}) when pid1 != pid2 do
    {:reply, :not_your_turn, state}
  end

  def handle_call(
        {:player_move, position}, {pid, _ref},
         state = %{board: board, current_player: pid}
      ) do
    {^pid, sign} = Enum.find(state.players, fn({pid1, _}) -> pid1 == pid  end)

    # cell_taken | invalid_play | game_over | draw | continue | ...
    {status, new_board} = Board.register_move(board, sign, position)
    case status do
      :invalid_play -> {:reply, :invalid_play, state}
      :cell_taken -> {:reply, :cell_taken, state}
      :game_over ->
        send(self(), :game_ended)
        {:reply, :you_won, %{state | board: new_board}}
      :draw ->
        send(self(), :game_ended)
        {:reply, :draw, %{state | board: new_board}}
      :continue -> {:reply, :ok, %{state | board: new_board, current_player: next_player(pid, state.players)}}
    end
  end

  def handle_call(:show_board, _from, state = %{board: board}) do
    {:reply, Board.print(board), state}
  end

  def handle_call({:join, _player_pid}, _from, state = %{players: players}) when length(players) == 2 do
    {:reply, :cant_join_game, state}
  end

  def handle_call({:join, player_pid}, _from, state = %{players: []}) do
    {
      :reply,
      @player_one_sign,
      %{state |
        players: [{player_pid, @player_one_sign}],
        current_player: player_pid
      }
    }
  end

  def handle_call({:join, player_pid}, _from, state = %{players: players}) do
    {:reply, @player_two_sign, %{state | players: [{player_pid, @player_two_sign} | players]}}
  end

  def handle_info(:game_ended, _state) do
    Process.exit(self(), :normal)
  end

  defp next_player(pid, players) do
    {next_pid, _} = Enum.find(players, fn({other_pid, _}) -> other_pid != pid end)
    next_pid
  end
end
