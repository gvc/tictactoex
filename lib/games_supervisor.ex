defmodule GamesSupervisor do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: {:global, __MODULE__})
  end

  def init(:ok) do
    Process.flag(:trap_exit, true)
    {:ok, %{
      available_game: nil,
      running_games: []
    }}
  end

  def join do
    GenServer.call({:global, __MODULE__}, :join)
  end

  def handle_call(:join, {player_pid, _ref}, state = %{available_game: game}) when is_nil(game) do
    {:ok, game_pid} = GameServer.start_link("qualquer coisa")
    GameServer.join(game_pid, player_pid)

    Process.monitor(game_pid)

    {:reply, game_pid, %{state | available_game: game_pid}}
  end

  # Coloca o cara no jogo e coloca o game no running games, atualiza o estado
  # Retorno deve ser o PID do jogo dele
  def handle_call(:join, {player_pid, _ref}, state = %{available_game: game, running_games: running_games}) do
    GameServer.join(game, player_pid)

    {:reply, game, %{available_game: nil, running_games: [game | running_games]}}
  end

  def handle_info({:DOWN, ref, :process, object, reason}, state) do
    IO.inspect ref
    IO.inspect object
    IO.inspect reason
    {:noreply, state}
  end

  def handle_info(args, state) do
    IO.puts("GENERAL HANDLE INFO")
    IO.inspect(args)
    {:noreply, state}
  end
end
